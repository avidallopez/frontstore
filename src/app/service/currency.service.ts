import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICurrency } from '../interface/ICurrency';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor(private http: HttpClient) { }

  public getCurrencyCOP(quantity: number): Observable<ICurrency>  {
      const params = new HttpParams().set('quantity', quantity.toString())
      .set('key', environment.currencyConfig.access_key);
      let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
      return this.http.get<ICurrency>(environment.apiCurrencyUrl, { params, headers
      });
  }

  public getCurrencyCOPBack(quantity: number): Observable<string>  {
    let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    let params = new HttpParams().set('value', quantity.toString());
    return this.http.get<string>(
      environment.urlBase + environment.storeUrl.currency,
      { params, headers }
    );
  }
}
