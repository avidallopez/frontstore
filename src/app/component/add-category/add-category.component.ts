import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { Router } from '@angular/router';
import { CategoryService } from '../../service/category.service';
import { ICategory } from '../../interface/ICategory';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {

  public categories: ICategory[];
  public category: ICategory;

  constructor(private authService: AuthService,
              private categoryService: CategoryService,
              private router: Router) {
                this.categories = [];
                this.category = {};
              }

  ngOnInit() {
    if (!this.authService.isAuthenticated()) {
      this.router.navigate(['/home']);
    }
    this.categoryService.getAll().subscribe(data => {
      if (data.length > 0) {
        this.categories = data;
      }
    });
  }

  public save(): void {
    this.categoryService.save(this.category).subscribe(data => {
      if (data) {
        alert('Categoria agregada de manera exitosa!!');
        this.router.navigate(['/admin/menu']);
      }
    }, err => {
      if (err.status == 401) {
        alert('su sesion ha expirado');
        this.authService.logout();
      }
    });
  }
}
