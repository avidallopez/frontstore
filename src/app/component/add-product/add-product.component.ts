import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { Router } from '@angular/router';
import { CategoryService } from '../../service/category.service';
import { ICategory } from '../../interface/ICategory';
import { IProduct } from '../../interface/IProduct';
import { ProductService } from '../../service/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  public categories: ICategory[];
  public product: IProduct;

  constructor(private authService: AuthService,
              private categoryService: CategoryService,
              private productService: ProductService,
              private router: Router) {
                this.categories = [];
                this.product = {};
              }

  ngOnInit() {
    if (!this.authService.isAuthenticated()) {
      this.router.navigate(['/home']);
    }

    this.categoryService.getSon().subscribe(data => {
      if (data.length > 0) {
        this.categories = data;
      }
    });
  }

  public save(): void {
    this.productService.save(this.product).subscribe(data => {
      if (data) {
        alert('Categoria agregada de manera exitosa!!');
        this.router.navigate(['/admin/menu']);
      }
    }, err => {
      if (err.status == 401) {
        alert('su sesion ha expirado');
        this.authService.logout();
      }
    })
  }

}
