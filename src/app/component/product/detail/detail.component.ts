import { Component, OnInit } from '@angular/core';
import { ProductComponent } from '../product.component';
import { ProductService } from '../../../service/product.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { IProduct } from '../../../interface/IProduct';
import { CurrencyService } from '../../../service/currency.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  public product: IProduct;
  public ant: number;
  public act: number;
  public id: number;
  public cop: number;

  constructor(private activatedRoute: ActivatedRoute,
              private productService: ProductService,
              private currencyService: CurrencyService,
              private router: Router) {
    this.product = {
      name: '',
      description: '',
      weigth: 0.0,
      price: 0.0,
    };
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.ant = params.anterior ? params.anterior : 0;
      this.act = params.siguiente ? params.siguiente : 0;
      this.id = params.id ? params.id : 0;
    });
    this.productService.getProduct(this.id).subscribe(data => {
      if (data) {
        this.product = data;
        /*this.currencyService.getCurrencyCOPBack(this.product.price).subscribe(cop => {
            console.log(cop);
        });*/
      }
    });
  }


  public getBack(): void {
    this.router.navigate(['/category/' + this.ant + '/' + this.act + '/product']);
  }

}
