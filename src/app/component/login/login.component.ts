import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public userName: string;
  public password: string;

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/admin/menu']);
    }
  }

  public login(): void {
    if (this.userName === null || this.password === null) {
      alert('Username o password vacías!');
    }
    this.authService.login(this.userName, this.password).subscribe(response => {
      localStorage.setItem('userToken', response.access_token);
      this.router.navigate(['/admin/menu']);
    }, err => {
      if (err.status == 400) {
        alert( 'Usuario o clave incorrectas!');
      }
    });
  }

}
