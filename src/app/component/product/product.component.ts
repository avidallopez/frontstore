import { Component, OnInit, ViewChild } from '@angular/core';
import { IProduct } from '../../interface/IProduct';
import { PaginatorComponent } from '../paginator/paginator.component';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { CategoryService } from '../../service/category.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  public products: IProduct[];
  public load: boolean;
  public ant: number;
  public act: number;

  @ViewChild(PaginatorComponent, {static: true})
  private paginatorComponent: PaginatorComponent;

  constructor(private activatedRoute: ActivatedRoute,
              private categoryService: CategoryService,
              private router: Router) {
      this.load = false;
      this.products = [];
    }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.ant = params.anterior ? params.anterior : 0;
      this.act = params.siguiente ? params.siguiente : 0;
    });
    setTimeout(() => {
      this.paginatorComponent.resetData();
      this.paginatorComponent.initPage();
      this.load = true;
    }, 500);
  }

  public showInfo($event: IProduct[]): void {
    this.products = $event;
  }

  public getBack(): void {
    this.load = false;
    if (this.ant > 0) {
      this.categoryService.getBack(this.ant).subscribe(data => {
          this.router.navigate(['/category/' + data + '/' + this.ant]);
      });
    } else {
      this.router.navigate(['/home']);
    }
  }

  public getDetalle(product: IProduct): void {
    this.router.navigate(['/category/' + this.ant + '/' + this.act + '/product/' + product.id]);
  }

}
