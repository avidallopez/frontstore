import { IRole } from './IRole';
export interface IUser {
    id?: string;
    name?: string;
    lastName?: string;
    userName?: string;
    password?: string;
    roles?: IRole[];
}