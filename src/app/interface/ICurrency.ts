export interface ICurrency {
    result?: {
        updated?: string,
        source?: string,
        target?: string,
        value?: number,
        quantity?: number;
        amount?: number;
      };
    status?: string;
}
