import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { ProductComponent } from './component/product/product.component';
import { HomeComponent } from './component/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { PaginatorComponent } from './component/paginator/paginator.component';
import { DetailComponent } from './component/product/detail/detail.component';
import { LoginComponent } from './component/login/login.component';
import { MenuAdminComponent } from './component/menu-admin/menu-admin.component';
import { FormsModule } from '@angular/forms';
import { AddProductComponent } from './component/add-product/add-product.component';
import { AddCategoryComponent } from './component/add-category/add-category.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ProductComponent,
    HomeComponent,
    PaginatorComponent,
    DetailComponent,
    LoginComponent,
    MenuAdminComponent,
    AddProductComponent,
    AddCategoryComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
