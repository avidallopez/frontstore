import { Component, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import { CategoryService } from '../../service/category.service';
import { ProductService } from '../../service/product.service';
import { ICategory } from '../../interface/ICategory';
import { IProduct } from '../../interface/IProduct';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent  {

  @Input()
  public typeService: string;
  @Input()
  public father: number;
  @Output()
  public outCategory = new EventEmitter<ICategory[]>();
  @Output()
  public outProduct = new EventEmitter<IProduct[]>();

  public pages: number[];
  public page: number;


  constructor(private categoryService: CategoryService, private productService: ProductService) {
    this.resetData();
  }

  public resetData(): void {
    this.page = 0;
    this.pages = [];
  }

  public initPage(): void {
    if (this.typeService === 'CA') {
      this.callToCategory();
    } else {
      this.callToProduct();
    }
  }

  public callToCategory(): void {
    if (this.father) {
      this.categoryService.getCategoryByFather(this.father, this.page).subscribe(
        data => {
          if (data.content.length > 0) {
            this.dataPaginator(data);
            this.outCategory.emit(data.content);
          } else {
            this.outCategory.emit([]);
          }
        }, () => {
          console.log('paila');
        });
    } else {
      this.categoryService.getCategories(this.page).subscribe(
        data => {
          if (data.content.length > 0) {
            this.dataPaginator(data);
            this.outCategory.emit(data.content);
          } else {
            this.outCategory.emit([]);
          }
        },
        () => {
          console.log('paila');
        });
    }
  }

  public callToProduct(): void {
    this.productService.getProductsByCategory(this.father, this.page).subscribe(
      data => {
        if (data.content.length > 0) {
          this.dataPaginator(data);
          this.outProduct.emit(data.content);
        } else {
          this.outProduct.emit([]);
        }
      });
  }

  private dataPaginator(data: any): void {
    this.pages = new Array(data.totalPages).fill(0).map((value, index) => index + 1);
  }

  goToPage(index: number): void {
    if (this.page !== index - 1) {
      this.page = index - 1;
      this.initPage();
    }
  }

}
