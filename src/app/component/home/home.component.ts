import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from 'src/app/service/category.service';
import { ICategory } from 'src/app/interface/ICategory';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PaginatorComponent } from '../paginator/paginator.component';
import { timeout } from 'q';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public categories: ICategory[];
  public ant: number;
  public act: number;
  public load: boolean;

  @ViewChild(PaginatorComponent, {static: true})
  private paginatorComponent: PaginatorComponent;

  constructor(private categoryService: CategoryService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.categories = [];
    this.load = false;
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.ant = params.anterior ? params.anterior : 0;
      this.act = params.siguiente ? params.siguiente : 0;
    });
    setTimeout(() => {
      this.paginatorComponent.resetData();
      this.paginatorComponent.initPage();
      this.load = true;
    }, 500);
  }

  public showInfo($event: ICategory[]): void {
    this.categories = $event;
  }

  public getBack(): void {
    this.load = false;
    if (this.ant > 0) {
      this.categoryService.getBack(this.ant).subscribe(data => {
          this.router.navigate(['/category/' + data + '/' + this.ant]);
          setTimeout(() => {
            this.paginatorComponent.resetData();
            this.paginatorComponent.initPage();
            this.load = true;
          }, 500);
      });
    } else {
      this.router.navigate(['/home']);
    }
  }

  public getDetalle(category: ICategory): void {
    this.load = false;
    this.categoryService.countSons(category.id).subscribe(response => {
      if (response > 0 ) {
        this.router.navigate(['/category/' + this.act + '/' + category.id]);
        setTimeout(() => {
          this.paginatorComponent.resetData();
          this.paginatorComponent.initPage();
        }, 500);
      } else {
        this.router.navigate(['/category/' + this.act + '/' + category.id + '/product']);
      }
      this.load = true;
    });
  }

}
