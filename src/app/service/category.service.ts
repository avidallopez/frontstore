import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ICategory } from '../interface/ICategory';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http: HttpClient) {}

  public getCategories(page: number): Observable<any> {
    let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    let params = new HttpParams().set('page', page.toString());
    return this.http.get<any>(
      environment.urlBase + environment.storeUrl.category,
      { params, headers }
    );
  }

  public getCategoryByFather(id: number, page: number): Observable<any> {
    let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    let params = new HttpParams().set('id', id.toString())
    .set('page', page.toString());
    return this.http.get<any>(
      environment.urlBase + environment.storeUrl.category + '/',
      { params, headers }
    );
  }

  public getBack(id: number): Observable<number> {
    let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    let params = new HttpParams().set('id', id.toString());
    return this.http.get<any>(
      environment.urlBase + environment.storeUrl.category + '/back',
      { params, headers }
    );
  }

  public countSons(id: number): Observable<number> {
    let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    let params = new HttpParams().set('id', id.toString());
    return this.http.get<any>(
      environment.urlBase + environment.storeUrl.category + '/sons',
      { params, headers }
    );
  }

  public getSon(): Observable<ICategory[]> {
    let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    return this.http.get<ICategory[]>(
      environment.urlBase + environment.storeUrl.category + '/son',
      { headers }
    );
  }

  public getAll(): Observable<ICategory[]> {
    let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    return this.http.get<ICategory[]>(
      environment.urlBase + environment.storeUrl.category + '/all',
      { headers }
    );
  }

  public save(category: ICategory): Observable<ICategory> {
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('userToken'))
    .set('Access-Control-Allow-Origin', '*');
    return this.http.post<ICategory>(environment.urlBase + environment.storeUrl.category, category, { headers });
  }
}
