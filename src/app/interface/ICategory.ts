export interface ICategory {
  id?: number;
  name?: string;
  father?: ICategory;
  photo?: string;
}
