// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlBase: 'http://localhost:8081',
  auth0: {
    login: '/oauth/token',
    user: '/user',
    authConfig: {
      grantType: 'password',
      clientId: 'StoreFrontApp',
      clientSecret: 'qwer1234'
    }
  },
  storeUrl: {
    category: '/category',
    producut: '/product',
    currency: '/currency'
  },
  apiCurrencyUrl: 'https://api.cambio.today/v1/quotes/USD/COP/json',
  currencyConfig: {
    access_key: '824|xn4ERpjeGjaXZi2o2f^nTw4^2XAFLDW4'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
