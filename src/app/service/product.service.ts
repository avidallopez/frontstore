import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { IProduct } from '../interface/IProduct';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {}

  public getProductsByCategory(id: number, page: number): Observable<any> {
    let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    let params = new HttpParams().set('id', id.toString())
    .set('page', page.toString());
    return this.http.get<any>(
      environment.urlBase + environment.storeUrl.producut,
      { params, headers }
    );
  }

  public getProduct(id: number): Observable<IProduct> {
    let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    let params = new HttpParams().set('id', id.toString());
    return this.http.get<IProduct>(
      environment.urlBase + environment.storeUrl.producut + '/detail',
      { params, headers }
    );
  }

  public save(product: IProduct): Observable<IProduct> {
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('userToken'))
    .set('Access-Control-Allow-Origin', '*');
    return this.http.post<IProduct>(environment.urlBase + environment.storeUrl.producut, product, { headers });
  }
}
