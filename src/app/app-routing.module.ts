import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../app/component/home/home.component';
import { ProductComponent } from '../app/component/product/product.component';
import { DetailComponent } from './component/product/detail/detail.component';
import { LoginComponent } from './component/login/login.component';
import { MenuAdminComponent } from './component/menu-admin/menu-admin.component';
import { AddCategoryComponent } from './component/add-category/add-category.component';
import { AddProductComponent } from './component/add-product/add-product.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'category/:anterior/:siguiente',
    component: HomeComponent
  },
  {
    path: 'category/:anterior/:siguiente/product',
    component: ProductComponent
  },
  {
    path: 'category/:anterior/:siguiente/product/:id',
    component: DetailComponent
  },
  {
    path: 'admin/menu',
    component: MenuAdminComponent
  },
  {
    path: 'admin/addProduct',
    component: AddProductComponent
  },
  {
    path: 'admin/addCategory',
    component: AddCategoryComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
