import { ICategory } from './ICategory';

export interface IProduct {
  id?: number;
  name?: string;
  description?: string;
  price?: number;
  priceCop?: number;
  photo?: string;
  category?: ICategory;
  weigth?: number;
}
