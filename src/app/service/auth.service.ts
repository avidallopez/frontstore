import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { logging } from 'protractor';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
              private router: Router) { }

  public login(userName: string, password: string): Observable<any> {
    const credentiales = btoa(environment.auth0.authConfig.clientId + ':' + environment.auth0.authConfig.clientSecret);
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Basic ' + credentiales
    });
    let params = new URLSearchParams();
    params.set('username', userName);
    params.set('password', password);
    params.set('grant_type', environment.auth0.authConfig.grantType);

    return this.http.post<any>(
      environment.urlBase + environment.auth0.login, params.toString(), { headers });
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('userToken');
    if ( !token ) {
      return false;
    }
    return true;
  }

  public logout(): void {
    localStorage.clear();
    localStorage.removeItem('userToken');
    this.router.navigate(['/home']);
  }

}
